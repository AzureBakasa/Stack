package cl.ubb.pila;

import static org.junit.Assert.*;

import org.junit.Test;

public class StackTest {

	@Test
	public void nuevaPilaVacio() {
		//arrange
		Pila pila = new Pila();
		int largo;
		//act
		largo=pila.size();
		//assert
		assertEquals(largo,0);
	}
	
	@Test
	//el test anterior asegura que al crear una pila inicia vacia
	public void apilarUnoEnPilaVacia() {
		//arrange
		Pila pila = new Pila();
		//act
		pila.apilar("1");
		//assert
		assertEquals(pila.get(0),"1");
	}

}
